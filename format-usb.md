# Resetting a USB

## Show partitions
```
lsblk
```

## Wipe USB
```
sudo dd if=/dev/zero of=/dev/disk bs=4k && sync  
sudo fdisk /dev/sda 
```
1. o
2. n
3. w

## Format FS
```
sudo mkfs.vfat /dev/diskX
```

## Eject USB
```
sudo eject /dev/disk
```

## Mount USB
```
sudo /dev/diskX /mnt/usb/
```

# Creating a live USB
Download .iso
```
sudo dd if=file.iso of=/dev/diskX bs=4M conv=fsync oflag=direct status=progress
```
