#!/bin/bash

# Reset live USB

sudo lsblk

read -rp "Enter the disk name (e.g., sda): " drive

read -rp "Using /dev/$drive. Type 'y' to confirm: " confirm
if [[ ! "$confirm" =~ ^[yY] ]]; then
    echo "Operation canceled."
    exit 1
fi

echo "Preparing /dev/$drive..."

sudo umount /dev/"${drive}"*
sudo wipefs -a /dev/"$drive"
sudo parted /dev/"$drive" --script mklabel msdos mkpart primary ext4 1MiB 100%
sudo mkfs.ext4 /dev/"${drive}1"

echo "Process complete. /dev/$drive has been reset for storage."
