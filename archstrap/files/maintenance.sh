#!/bin/bash

# Define color codes and status symbols
GREEN="\e[32m"
RED="\e[31m"
YELLOW="\e[33m"
RESET="\e[0m"
CHECK="\xE2\x9C\x85"   # ✅
CROSS="\xE2\x9D\x8C"   # ❌

# Status variables
mirror_status=""
keys_status=""
prune_status=""
orphans_status=""
cache_status=""
docker_status=""

set -x

# Update mirror list
echo -e "${YELLOW}Updating mirror list...${RESET}"
if sudo reflector --verbose --latest 10 --sort rate --save /etc/pacman.d/mirrorlist; then
    mirror_status="${GREEN}${CHECK} mirror list updated${RESET}"
else
    mirror_status="${RED}${CROSS} mirror list update failed${RESET}"
fi

# Update and refresh keys if failed
echo -e "${YELLOW}Updating packages and keys...${RESET}"
if yay --noconfirm; then
    keys_status="${GREEN}${CHECK} packages updated${RESET}"
else
    if sudo pacman-keys --refresh-keys && yay --noconfirm; then
        keys_status="${GREEN}${CHECK} packages updated and keys refreshed${RESET}"
    else
        keys_status="${RED}${CROSS} package update and key refresh failed${RESET}"
    fi
fi

# Prune cache
echo -e "${YELLOW}Pruning cache...${RESET}"
if yes | sudo paccache -rk1; then
    prune_status="${GREEN}${CHECK} cache pruned${RESET}"
else
    prune_status="${RED}${CROSS} cache prune failed${RESET}"
fi

# Remove orphans
echo -e "${YELLOW}Removing orphaned packages...${RESET}"
set -e
sudo pacman -Qtdq | xargs sudo pacman -Rns --noconfirm
set +e

# Clean cache directory
echo -e "${YELLOW}Cleaning cache directories...${RESET}"
if rm -rf ~/.cache/* && sudo rm -rf /tmp/*; then
    cache_status="${GREEN}${CHECK} cache cleaned${RESET}"
else
    cache_status="${RED}${CROSS} cache directory clean-up failed${RESET}"
fi

# Cleanup docker objects
echo -e "${YELLOW}Cleaning Docker objects...${RESET}"
if docker system prune -af; then
    docker_status="${GREEN}${CHECK} docker cleaned${RESET}"
else
    docker_status="${RED}${CROSS} docker clean-up failed${RESET}"
fi

# Scan for rootkits and suspicious files
sudo rkhunter --propupd 2>&1 | grep -Ev '^(grep|egrep): warning:'
sudo rkhunter --update 2>&1 | grep -Ev '^(grep|egrep): warning:'
if sudo rkhunter --check --sk --rwo --quiet 2>&1 | grep -Ev '^(grep|egrep): warning:' | grep -q '.'; then
    rkhunter_status="${RED}${CROSS} rkhunter check failed... possible security issue detected!${RESET}"
else
    rkhunter_status="${GREEN}${CHECK} rkhunter passed${RESET}"
fi

# update rust
if rustup update; then
    rust_status="${GREEN}${CHECK} rust updated${RESET}"
else
    rust_status="${RED}${CROSS} rust update failed${RESET}"
fi


# Summary
set +x
echo -e "\n${YELLOW}Summary:${RESET}"
echo -e "$mirror_status"
echo -e "$keys_status"
echo -e "$prune_status"
echo -e "$orphans_status"
echo -e "$cache_status"
echo -e "$docker_status"
echo -e "$rkhunter_status"
