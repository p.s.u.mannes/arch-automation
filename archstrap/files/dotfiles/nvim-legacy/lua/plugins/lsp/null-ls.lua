local setup, null_ls = pcall(require, "null-ls")
if not setup then
  return
end

local formatting = null_ls.builtins.formatting -- to setup formatters
local diagnostics = null_ls.builtins.diagnostics -- to setup linters

-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins
null_ls.setup({
  -- setup formatters & linters
  sources = {
    formatting.black,
    diagnostics.flake8,
    diagnostics.yamllint,
    diagnostics.ansiblelint,
  },
})
