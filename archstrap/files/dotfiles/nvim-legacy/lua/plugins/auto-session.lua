local setup, auto_session = pcall(require, "auto-session")
if not setup then
    return
end

auto_session.setup({
    auto_save_enabled = true,
    auto_restore_enabled = true,
    log_level = "error",
})
