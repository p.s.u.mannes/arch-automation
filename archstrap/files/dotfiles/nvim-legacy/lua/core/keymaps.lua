-- get all keybinds: :nmap

vim.g.mapleader = " "

local keymap = vim.keymap -- for conciseness

-- NVIM COMMANDS

-- insert mode when terminal is opened
vim.api.nvim_command('autocmd TermOpen * startinsert')

-- GENERAL KEYMAPS

-- INSERT MODE
keymap.set("i", "jk", "<ESC>")

-- autoclose
-- vim.api.nvim_set_keymap('i', "'", "''<left>", { noremap = true })
-- vim.api.nvim_set_keymap('i', '"', '""<left>', { noremap = true })
-- vim.api.nvim_set_keymap('i', '<', '<><left>', { noremap = true })
-- vim.api.nvim_set_keymap('i', '(', '()<left>', { noremap = true })
-- vim.api.nvim_set_keymap('i', '[', '[]<left>', { noremap = true })
-- vim.api.nvim_set_keymap('i', '{', '{}<left>', { noremap = true })

-- NORMAL MODE
-- clear search highlight
keymap.set("n", "<leader>nh", ":nohl<CR>")

-- delete char without copying into register
keymap.set("n", "x", '"_x')

-- WINDOWS
-- window split with old buffer
-- :sv
-- :sp
-- window split with new buffer (for unique window)
keymap.set("n", "<leader>sp", ":new<CR>")
keymap.set("n", "<leader>sv", ":vnew<CR>")
keymap.set("n", "<leader>sx", ":clear<CR>")

-- TABS
-- :tabe
-- gt
-- gT

-- PLUGIN KEYMAPS

-- vim-maximizer
keymap.set("n", "<leader>sm", ":MaximizerToggle<CR>")

-- nvim-tree, <CR> = ENTER
keymap.set("n", "<leader>e", ":NvimTreeToggle<CR>")

-- TELESCOPE
-- find files
keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<cr>")
-- find string
keymap.set("n", "<leader>fs", "<cmd>Telescope live_grep<cr>")
-- find cursor string
keymap.set("n", "<leader>fc", "<cmd>Telescope grep_string<cr>")
-- find buffers
keymap.set("n", "<leader>fb", "<cmd>Telescope buffers<cr>")
-- find help
keymap.set("n", "<leader>fh", "<cmd>Telescope help_tags<cr>")

-- git status
keymap.set("n", "<leader>gs", "<cmd>Telescope git_status<cr>")
-- git commits
keymap.set("n", "<leader>gc", "<cmd>Telescope git_commits<cr>")
-- git branches
keymap.set("n", "<leader>gb", "<cmd>Telescope git_branches<cr>")

--TERMINAL
-- quit the terminal
keymap.set('t', '<leader><Esc>', '<C-\\><C-n>', { noremap = true })
-- open terminal mode
keymap.set('n', '<leader>t', ':terminal<CR>', { noremap = true })

-- BUFFERS
-- delete all buffers except current buffer
keymap.set('n', '<leader>db', ':%bd!|e#<CR>', { silent = true })

-- HANDY REMAPS
-- faster command access
keymap.set('n', ';', ':', { noremap = true })
-- paste what was copied, not deleted 
keymap.set('n', ',p', '"0p', { noremap = true })

-- VISUAL MODE --
-- Stay in indent mode
keymap.set("v", "<", "<gv", opts)
keymap.set("v", ">", ">gv", opts)
