# README

### Setup on debian
```
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/Meslo.zip
unzip Meslo.zip -d Meslo
sudo cp Meslo/'Meslo LG S Regular Nerd Font Complete.ttf' /usr/share/fonts/truetype/
sudo fc-cache -f -v
sudo apt install ripgrep nodejs npm python3-venv

```
### WSL setup
curl -sLo /tmp/win32yank.zip https://github.com/equalsraf/win32yank/releases/download/v0.0.4/win32yank-x64.zip
unzip -p /tmp/win32yank.zip win32yank.exe > /tmp/win32yank.exe
chmod +x /tmp/win32yank.exe
sudo mv /tmp/win32yank.exe /usr/local/bin/

### WSL font
https://github.com/powerline/fonts/blob/master/DejaVuSansMono/DejaVu%20Sans%20Mono%20for%20Powerline.ttf

### LSP and linters

```
:PackerSync
:MasonInstall <NameOfLinter/Formatter>
```

### Shortcut examples

```
C- o: next buffer
C- i: prev buffer

C- ws split window
C- wv split window vertical

gf: get function definition, implementation, references

] d: jump to code error

<leader>ca: code action to fix error

<leader>rn: rename+refactor function/variable

:wa: save changes across all files

```

