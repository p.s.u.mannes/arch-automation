#!/bin/sh

run() {
    if ! pgrep -f "$1"; then
        "$@" &
    fi
}

run "lxsession"
run "pavucontrol"
run "firefox"

