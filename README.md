# Automated Arch Install and Dotfile Backup
&nbsp; 
<img src="https://gitlab.com/p.s.u.mannes/archstrap/-/raw/master/public/ansible_arch.png" alt="Arch logo">&nbsp; 

### License
[GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html)
&nbsp; 

### Howto
- Download the latest arch Linux .iso. 
<https://wiki.archlinux.org/title/USB_flash_installation_medium>
```sh 
aria2c torrent_file
sudo lsblk
sudo dd bs=4M if=archlinux-2024.11.01-x86_64.iso of=/dev/sdX
```


- Connect to wifi if not wired
```
iwctl
device list
station [interface] scan
station [interface] get-networks
station [interface] connect [network name]
station [interface] show
```
- Run the installer
```
curl -sL https://gitlab.com/p.s.u.mannes/arch-automation/-/raw/master/init > init
sh init
```

### Wifi after reboot
```
# connect to wifi
nmcli device wifi list
nmcli device wifi connect <SSID> password <password>
nmcli device status
```
&nbsp; 

### VBox testing
```
Machine settings, enable EFI.
```
&nbsp; 
