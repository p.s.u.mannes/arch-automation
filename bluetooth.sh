#!/bin/bash

# configure as needed

bluetoothctl power on
bluetoothctl discoverable on
# bluetoothctl scan on
bluetoothctl devices

# bluetoothctl connect MAC_ADDR
bluetoothctl connect
